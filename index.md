---
layout: home
---

# Tổng hợp

- [Trello nhóm](https://trello.com/invite/b/0JMn3G5J/fe981e03cf3fc4aa5820c27de44dbfda/lu%E1%BA%ADn-v%C4%83n-ch%E1%BB%A5p-%E1%BA%A3nh-gi%E1%BA%A3i-tr%C3%AD)
- [Gitlab repo](https://gitlab.com/nk-hcmus/magiccam)
- [Google Drive chứa các sản phẩm của luận văn](https://drive.google.com/drive/folders/1w9QIWaxLMInLclIj0cKc5DtLDarkOuU7?usp=sharing)


# Cập nhật quá trình

## 28/10/2020

- [Fantasy Face v01.apk](assets/2020-10-28/fantasy-face-v01.apk)

- [Gender classification Server v01](assets/2020-10-28/gender-classification-server-master.zip)

- [Preprocess face Server v01](assets/2020-10-28/preprocess-face-server-master.zip)

- [Stargan-v2 v01](assets/2020-10-28/stargan-v2.rar)

- [Release note](assets/2020-10-28/release_note.txt)

## 14/10/2020

- [Đề cương khóa luận v1.0](assets/2020-10-14/De_Cuong_Chup_Anh_Giai_Tri.pdf)

- [Release note](assets/2020-10-14/release_note.txt) 

## 07/10/2020

- [Đề cương khóa luận v0.2](assets/2020-10-07/De_Cuong_Luan_Van.pdf)

- [Release note](assets/2020-10-07/release_note.txt)

## 30/09/2020

- [Slide đề cương khóa luận](assets/2020-09-30/De_Cuong_Khoa_Luan.pptx)

- [Release note](assets/2020-09-30/release_note.txt)